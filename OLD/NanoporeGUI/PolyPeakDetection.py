from PyQt5 import QtWidgets as qtw
from PyQt5.QtCore import Qt
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import pyabf
from scipy.signal import find_peaks
import numpy.polynomial.polynomial as poly
from MplCanvas import MplCanvas


class PolyPeakDetection(qtw.QWidget):

    def __init__(self, parent):
        # internal variables
        self.current = []
        self.time = []

        # tab layout
        super(qtw.QWidget, self).__init__(parent)
        layout = qtw.QVBoxLayout(self)

        # TITLE
        title = qtw.QLabel("Nanopore Peak Detection, polynomial line of best fit, peaks over threshold away from curve")
        title.setTextInteractionFlags(Qt.TextSelectableByMouse)
        title.setFixedHeight(24)
        layout.addWidget(title)

        # DATA BOX - includes graph parameters and results
        data = qtw.QHBoxLayout()

        # PARAMETER BOX
        parameters = qtw.QGroupBox("parameters")
        parameters.setFixedHeight(128)
        parameter_grid = qtw.QGridLayout()

        # peaks selection
        selection_label = qtw.QLabel("peak selection:")
        self.positive_checkbox = qtw.QCheckBox("peaks")
        self.negative_checkbox = qtw.QCheckBox("minima")
        self.positive_checkbox.setChecked(True)
        self.negative_checkbox.setChecked(True)

        # threshold
        threshold_label = qtw.QLabel("threshold from line of best fit:")
        self.threshold_value = qtw.QLineEdit("10")

        # add to parameter box
        parameter_grid.addWidget(selection_label, 0, 0)
        parameter_grid.addWidget(self.positive_checkbox, 0, 1)
        parameter_grid.addWidget(self.negative_checkbox, 0, 2)
        parameter_grid.addWidget(threshold_label, 1, 0)
        parameter_grid.addWidget(self.threshold_value, 1, 1)
        parameters.setLayout(parameter_grid)

        # RESULTS BOX
        results = qtw.QGroupBox("results")
        results.setFixedHeight(128)
        results_grid = qtw.QGridLayout()

        # peaks totals and current averages
        self.total_events_label = qtw.QLabel("total events:")
        self.total_events_value = qtw.QLabel("0")
        self.total_peaks_label = qtw.QLabel("total peaks:")
        self.total_peaks_value = qtw.QLabel("0")
        self.total_minima_label = qtw.QLabel("total minima:")
        self.total_minima_value = qtw.QLabel("0")
        self.mean_current_label = qtw.QLabel("mean current:")
        self.mean_current_value = qtw.QLabel("0")
        self.sigma_label = qtw.QLabel("standard deviation:")
        self.sigma_value = qtw.QLabel("0")
        self.variance_label = qtw.QLabel("variance:")
        self.variance_value = qtw.QLabel("0")

        # add to results box
        results_grid.addWidget(self.total_events_label, 0, 0)
        results_grid.addWidget(self.total_events_value, 0, 1)
        results_grid.addWidget(self.total_peaks_label, 1, 0)
        results_grid.addWidget(self.total_peaks_value, 1, 1)
        results_grid.addWidget(self.total_minima_label, 2, 0)
        results_grid.addWidget(self.total_minima_value, 2, 1)
        results_grid.addWidget(self.mean_current_label, 0, 2)
        results_grid.addWidget(self.mean_current_value, 0, 3)
        results_grid.addWidget(self.sigma_label, 1, 2)
        results_grid.addWidget(self.sigma_value, 1, 3)
        results_grid.addWidget(self.variance_label, 2, 2)
        results_grid.addWidget(self.variance_value, 2, 3)

        results.setLayout(results_grid)

        # add to layout
        data.addWidget(parameters, 1)
        data.addWidget(results, 1)
        layout.addLayout(data)

        # GRAPH
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        toolbar = NavigationToolbar(self.canvas, self)
        layout.addWidget(toolbar)
        layout.addWidget(self.canvas)

        self.setLayout(layout)

        # connections
        self.positive_checkbox.stateChanged.connect(self.update_graph)
        self.negative_checkbox.stateChanged.connect(self.update_graph)
        self.threshold_value.editingFinished.connect(self.update_graph)

    def set_input(self, time, current):
        self.time = time
        self.current = current

    def update_graph(self):
        #clear canvas
        self.canvas.axes1.clear()

        # polynomial regression (curve of best fit)
        coefficients = poly.polyfit(self.time, self.current, 8)
        poly_fit = poly.polyval(self.time, coefficients)
        threshold = float(self.threshold_value.text())
        self.canvas.axes1.plot(self.time, poly_fit, color='b')
        self.canvas.axes1.plot(self.time, poly_fit+threshold, color='b', alpha=0.5)
        self.canvas.axes1.plot(self.time, poly_fit-threshold, color='b', alpha=0.5)
        self.canvas.axes1.plot(self.time, self.current, alpha=0.5)

        # peak detection
        if self.positive_checkbox.isChecked():
            peaks = find_peaks(self.current, height=poly_fit+threshold)
            positive_heights = self.current[peaks[0]]
            peak_positions = self.time[peaks[0]]
            self.canvas.axes1.scatter(peak_positions, positive_heights, color='r', s=15, marker='D', label='Maxima')
            # update total
            total = len(peaks[0])
            self.total_peaks_value.setText(str(total))
        else:
            self.total_peaks_value.setText("0")

        if self.negative_checkbox.isChecked():
            minima = find_peaks(self.current * -1, height=(poly_fit-threshold) * -1)
            negative_heights = self.current[minima[0]]
            minima_positions = self.time[minima[0]]
            self.canvas.axes1.scatter(minima_positions, negative_heights, color='g', s=15, marker='D', label='Maxima')
            # update total
            total = len(minima[0])
            self.total_minima_value.setText(str(total))
        else:
            self.total_minima_value.setText("0")

        # update grand total
        total_events = int(self.total_peaks_value.text()) + int(self.total_minima_value.text())
        self.total_events_value.setText(str(total_events))

        self.canvas.draw()
