import sys
from PyQt5 import QtWidgets as qtw
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtCore import Qt
from InputTab import InputTab
from NormPeakDetection import NormPeakDetection
from PolyPeakDetection import PolyPeakDetection
from ProminencePeakDetection import ProminencePeakDetection
import pyabf


class TabWidget(qtw.QWidget):

    def __init__(self, parent):
        super(qtw.QWidget, self).__init__(parent)

        # store input data
        self.current = []
        self.time = []

        # tab widget layout
        layout = qtw.QVBoxLayout(self)
        tabs = qtw.QTabWidget()
        self.tab1 = InputTab(self)
        self.tab2 = NormPeakDetection(self)
        self.tab3 = PolyPeakDetection(self)
        self.tab4 = ProminencePeakDetection(self)
        tabs.addTab(self.tab1, "File Input")
        tabs.addTab(self.tab2, "Peak Detection 1")
        tabs.addTab(self.tab3, "Peak Detection 2")
        tabs.addTab(self.tab4, "Peak Detection 3")
        layout.addWidget(tabs)
        self.setLayout(layout)
        self.make_labels_selectable()

        # connections
        self.tab1.clicked.connect(self.set_input_data)

    def set_input_data(self):
        # update all tabs with the new file location
        response, ok = QFileDialog.getOpenFileName(self)
        abf = pyabf.ABF(response)
        abf.setSweep(0)

        self.tab1.show_filename(response)
        self.tab1.progress_bar.setValue(1)
        self.tab2.set_input(time=abf.sweepX, current=abf.sweepY)
        self.tab1.progress_bar.setValue(2)
        self.tab3.set_input(time=abf.sweepX, current=abf.sweepY)
        self.tab1.progress_bar.setValue(3)
        self.tab4.set_input(time=abf.sweepX, current=abf.sweepY)
        self.tab1.progress_bar.setValue(4)
        self.tab2.update_graph()
        self.tab1.progress_bar.setValue(6)
        self.tab3.update_graph()
        self.tab1.progress_bar.setValue(8)
        self.tab4.update_graph()
        self.tab1.progress_bar.setValue(10)

    def make_labels_selectable(self):
        labels = self.findChildren(qtw.QLabel)
        for i in labels:
            i.setTextInteractionFlags(Qt.TextSelectableByMouse)


class Ui(qtw.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(qtw.QMainWindow, self).__init__(*args, **kwargs)

        self.setMinimumSize(1000, 600)
        self.setWindowTitle("Nanopore Signal Peak Detection")

        table_widget = TabWidget(self)
        self.setCentralWidget(table_widget)

        self.show()


# run the application
app = qtw.QApplication(sys.argv)
window = Ui()
window.show()
app.exec_()
