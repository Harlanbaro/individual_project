from PyQt5 import QtWidgets as qtw
from PyQt5.QtCore import pyqtSignal, Qt


class InputTab(qtw.QWidget):

    clicked = pyqtSignal()

    def __init__(self, parent):
        super(qtw.QWidget, self).__init__(parent)
        layout = qtw.QVBoxLayout(self)

        # Intro & file input
        intro = qtw.QLabel("Directions on how to use this software.... etc")
        intro.setFixedHeight(24)

        file_button = qtw.QPushButton("Browse")
        file_button.setFixedWidth(128)
        self.file_location = qtw.QLabel()
        self.file_location.setStyleSheet("border: 1px solid black;")
        self.progress_bar = qtw.QProgressBar()
        self.progress_bar.setValue(0)
        self.progress_bar.setMaximum(10)
        box = qtw.QHBoxLayout()
        box.addWidget(file_button)
        box.addWidget(self.file_location)

        layout.addWidget(intro)
        layout.addLayout(box)
        layout.addWidget(self.progress_bar)
        layout.addStretch()

        # connections
        file_button.clicked.connect(self.clicked)

    def show_filename(self, file):
        self.file_location.setText(file)
