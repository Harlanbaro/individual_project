from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure


class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100, subplots=1):
        fig = Figure(figsize=(width, height), dpi=dpi)

        if subplots == 1:
            self.axes1 = fig.add_subplot(111)
        elif subplots == 2:
            self.axes1 = fig.add_subplot(121)
            self.axes2 = fig.add_subplot(122)

        super(MplCanvas, self).__init__(fig)
        fig.tight_layout()