#.rst:
# Findmshr
# --------
#
# Find mshr library
#
# Find the mshr includes and library. This module defines
#
# ::
#
#   mshr_INCLUDE_DIRS, where to find mshr.h.
#   mshr_LIBRARIES, libraries to link against to use mshr
#   mshr_FOUND, If false (0), do not try to use mshr.
#
#
#
#
#
#=============================================================================
find_path(mshr_INCLUDE_DIR mshr.h
          DOC "The mshr include directory")

set(mshr_NAMES ${mshr_NAMES} libmshr mshr)
find_library(mshr_LIBRARY NAMES ${mshr_NAMES}
            DOC "The mshr library")

# handle the QUIETLY and REQUIRED arguments and set mshr_FOUND to TRUE if
# all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(mshr
                                  REQUIRED_VARS mshr_LIBRARY
                                                mshr_INCLUDE_DIR
                                  VERSION_VAR mshr_VERSION_STRING)

if(mshr_FOUND)
  # FIND_PACKAGE_HANDLE_STANDARD_ARGS sets mshr_FOUND to TRUE not 1
  # which interferes with the json in config.json (must be true not TRUE)
  # So set to 1 (following find_package(DOLFIN) )
  set( mshr_FOUND 1 )
  # use by setuptools.Extension, mshr_LIBRARIES must be in a form that appends to -l
  # i.e. mshr not libmshr.so
  set( mshr_LIBRARIES "mshr" )
  get_filename_component( mshr_LIBRARIES_DIRS ${mshr_LIBRARY} DIRECTORY )
  set( mshr_INCLUDE_DIRS ${mshr_INCLUDE_DIR} )
else()
  set( mshr_FOUND 0)
  set( mshr_LIBRARIES_DIRS "." )
  set( mshr_INCLUDE_DIRS "." )
endif()

mark_as_advanced(mshr_INCLUDE_DIR mshr_LIBRARY)
