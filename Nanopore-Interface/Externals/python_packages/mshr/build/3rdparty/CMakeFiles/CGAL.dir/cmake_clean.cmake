file(REMOVE_RECURSE
  "CMakeFiles/CGAL"
  "CMakeFiles/CGAL-complete"
  "src/CGAL-stamp/CGAL-build"
  "src/CGAL-stamp/CGAL-configure"
  "src/CGAL-stamp/CGAL-download"
  "src/CGAL-stamp/CGAL-install"
  "src/CGAL-stamp/CGAL-mkdir"
  "src/CGAL-stamp/CGAL-patch"
  "src/CGAL-stamp/CGAL-update"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/CGAL.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
