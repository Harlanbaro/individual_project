# - Config file for the mshr package
# It defines the following variables
#  MSHR_INCLUDE_DIRS  - include directories for mshr
#  MSHR_LIBRARIES_DIR - directory where the mshr library is located
#  MSHR_LIBRARIES     - libraries to link against
 
# Compute paths
get_filename_component(MSHR_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
set(MSHR_INCLUDE_DIRS "/usr/local/include")
set(MSHR_EXTERNAL_INCLUDE_DIRS "/usr/local/include;;")
set(MSHR_LIBRARIES_DIRS "/usr/local/lib")
set(MSHR_EXTERNAL_LIBRARIES "dolfin;")
set(MSHR_CXX_DEFINITIONS "-DDOLFIN_VERSION=\"2019.1.0\";")
set(MSHR_CXX_FLAGS "")
set(MSHR_LIBRARIES mshr)

set(MSHR_USE_FILE "${MSHR_CMAKE_DIR}/use-mshr.cmake")
