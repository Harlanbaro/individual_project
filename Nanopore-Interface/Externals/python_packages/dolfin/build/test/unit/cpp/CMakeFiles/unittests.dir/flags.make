# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.22

# compile CXX with /usr/bin/c++
CXX_DEFINES = -DDOLFIN_VERSION=\"2019.1.0\" -DHAS_CHOLMOD -DHAS_HDF5 -DHAS_MPI -DHAS_PETSC -DHAS_SCOTCH -DHAS_SLEPC -DHAS_UMFPACK -DHAS_ZLIB -DNDEBUG

CXX_INCLUDES = -I/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin -I/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/dolfin -I/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/build -I/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/test/unit/cpp/catch -isystem /home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/venv/lib/python3.8/site-packages/ffc/backends/ufc -isystem /usr/include/eigen3 -isystem /usr/include/hdf5/openmpi -isystem /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi -isystem /usr/lib/x86_64-linux-gnu/openmpi/include -isystem /usr/lib/slepcdir/slepc3.12/x86_64-linux-gnu-real/include -isystem /usr/lib/petscdir/petsc3.12/x86_64-linux-gnu-real/include

CXX_FLAGS = -O2 -g -DNDEBUG -std=c++11

