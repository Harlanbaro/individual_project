# CMake generated Testfile for 
# Source directory: /home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/demo/documented/poisson/cpp
# Build directory: /home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/build/demo/documented/poisson/cpp
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(demo_poisson_mpi "mpirun" "-np" "3" "./demo_poisson")
set_tests_properties(demo_poisson_mpi PROPERTIES  _BACKTRACE_TRIPLES "/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/demo/documented/poisson/cpp/CMakeLists.txt;42;add_test;/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/demo/documented/poisson/cpp/CMakeLists.txt;0;")
add_test(demo_poisson_serial "/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/build/demo/documented/poisson/cpp/demo_poisson")
set_tests_properties(demo_poisson_serial PROPERTIES  _BACKTRACE_TRIPLES "/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/demo/documented/poisson/cpp/CMakeLists.txt;43;add_test;/home/harlanb/UNI/Individual_Project/individual_project/Nanopore-Interface/Externals/python_packages/dolfin/demo/documented/poisson/cpp/CMakeLists.txt;0;")
