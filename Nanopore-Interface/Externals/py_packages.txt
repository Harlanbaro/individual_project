GUI:
pyqt5
numpy
scipy
pyabf

Simulation:
mrdna (from source)
fenics
mshr (not installed)
scikit-image
griddata (mdanalysis?)
dill

mrdna:
mdanalysis
cadnano (from source)
appdirs
