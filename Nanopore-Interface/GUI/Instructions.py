from PyQt5 import QtWidgets as qtw
from PyQt5.QtCore import pyqtSignal, Qt, QTimer


class Instructions(qtw.QWidget):

    clicked = pyqtSignal()
    output_ready = pyqtSignal()

    def __init__(self, parent):
        super(qtw.QWidget, self).__init__(parent)
        layout = qtw.QVBoxLayout(self)

        # Intro 
        intro = qtw.QLabel("Please see https://gitlab.com/Harlanbaro/individual_project for detailed instructions on how to use this software")
        intro.setFixedHeight(24)

        layout.addWidget(intro)
        layout.addStretch()
