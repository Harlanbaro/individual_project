import sys
from PyQt5 import QtWidgets as qtw
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtCore import Qt
from scipy.signal import find_peaks
import pyabf
from Instructions import Instructions
from PeakDetection import PeakDetection
from Simulation import Simulation


class TabWidget(qtw.QWidget):

    def __init__(self, parent):
        super(qtw.QWidget, self).__init__(parent)

        # tab widget layout
        layout = qtw.QVBoxLayout(self)
        tabs = qtw.QTabWidget()
        
        self.input_tab = Instructions(self)
        self.peak_detection_tab = PeakDetection(self)
        self.simulation_tab = Simulation(self)
        
        tabs.addTab(self.input_tab, "Instructions")
        tabs.addTab(self.peak_detection_tab, "Peak Detection")
        tabs.addTab(self.simulation_tab, "Simulation")

        layout.addWidget(tabs)
        self.setLayout(layout)
        self.make_labels_selectable()

    def make_labels_selectable(self):
        labels = self.findChildren(qtw.QLabel)
        for i in labels:
            i.setTextInteractionFlags(Qt.TextSelectableByMouse)


class Ui(qtw.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(qtw.QMainWindow, self).__init__(*args, **kwargs)

        self.setMinimumSize(1000, 700)
        self.setWindowTitle("Nanopore Signal Peak Detection")

        table_widget = TabWidget(self)
        self.setCentralWidget(table_widget)

        #self.show()


# run the application
if __name__ == "__main__":
    app = qtw.QApplication(sys.argv)
    window = Ui()
    window.show()
    app.exec_()
