from PyQt5 import QtWidgets as qtw
from PyQt5.QtCore import pyqtSignal, Qt, QTimer
import matplotlib
from matplotlib.collections import PolyCollection
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import pyabf
from scipy.stats import norm
from scipy.signal import find_peaks
import numpy as np
import pandas as pd
from MplCanvas import MplCanvas
import os
import subprocess




class Simulation(qtw.QWidget):

    def __init__(self, parent):
        # data arrays
        self.current = []
        self.time = []

        # tab layout
        super(qtw.QWidget, self).__init__(parent)
        layout = qtw.QVBoxLayout(self)

        # TITLE
        title = qtw.QLabel("Simulation Output")
        title.setFixedHeight(24)

        # Simulation
        simulation = qtw.QGroupBox("Simulation ")
        simulation.setFixedHeight(250)
        simulation_grid = qtw.QGridLayout()

        # model file input
        model_label = qtw.QLabel("Simulation Model File:")
        model_button = qtw.QPushButton("Browse")
        self.model_location = qtw.QLabel()
        self.model_location.setStyleSheet("border: 1px solid black;")

        # control headers
        simulation_label = qtw.QLabel("Simulation control")
        simulation_label.setStyleSheet("font-weight: bold")
        output_label = qtw.QLabel("Output controls")
        output_label.setStyleSheet("font-weight: bold")

        # simulation control buttons
        clear_button = qtw.QPushButton("Clear Simulation Files")
        self.start_simulation_button = qtw.QPushButton("Run Simulation")
        stop_simulation_button = qtw.QPushButton("Stop Simulation")
        visualise_button = qtw.QPushButton("Visualise Simulation")
        
        # output control buttons
        first_step_label = qtw.QLabel("first step")
        last_step_label = qtw.QLabel("last step")
        first_step_value = qtw.QLineEdit()
        last_step_value = qtw.QLineEdit()
        self.start_output_button = qtw.QPushButton("Output Data")
        stop_output_button = qtw.QPushButton("Stop Output")
        test_button = qtw.QPushButton("test display")

        # add to simulation grid
        simulation_grid.addWidget(model_label, 0, 0)
        simulation_grid.addWidget(self.model_location, 0, 1, 1, 4)
        simulation_grid.addWidget(model_button, 0, 5)
        
        simulation_grid.addWidget(simulation_label, 1, 0, 1, 2)
        simulation_grid.addWidget(output_label, 1, 2)

        simulation_grid.addWidget(clear_button, 2, 0, 1, 2)
        simulation_grid.addWidget(self.start_simulation_button, 3, 0, 1, 2)
        simulation_grid.addWidget(visualise_button, 4, 0, 1, 2)
        simulation_grid.addWidget(stop_simulation_button, 5, 0, 1, 2)

        simulation_grid.addWidget(first_step_label, 2, 2)
        simulation_grid.addWidget(first_step_value, 2, 3)
        simulation_grid.addWidget(last_step_label, 2, 4)
        simulation_grid.addWidget(last_step_value, 2, 5)
        simulation_grid.addWidget(self.start_output_button, 3, 2, 1, 4)
        simulation_grid.addWidget(stop_output_button, 4, 2, 1, 4)
        simulation_grid.addWidget(test_button, 5, 2, 1, 4)
        
        simulation_grid.setColumnStretch(0, 2)
        simulation_grid.setColumnStretch(1, 2)
        simulation_grid.setColumnStretch(2, 1)
        simulation_grid.setColumnStretch(3, 1)
        simulation_grid.setColumnStretch(4, 1)
         
        simulation.setLayout(simulation_grid)

        # GRAPH
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100, subplots=1)
        toolbar = NavigationToolbar(self.canvas, self)
        
        # layout
        layout.addWidget(title)
        layout.addWidget(simulation)
        layout.addWidget(toolbar)
        layout.addWidget(self.canvas)

        self.setLayout(layout)

        # timer for simulation
        self.output_timer = QTimer(self)
        self.output_timer.setInterval(60000) # once a minute update to hour on

        # connections
        clear_button.clicked.connect(self.clear)
        self.start_simulation_button.clicked.connect(self.start_simulation)
        stop_simulation_button.clicked.connect(self.stop_simulation)   
        visualise_button.clicked.connect(self.visualise)
        self.start_output_button.clicked.connect(self.start_output)
        stop_output_button.clicked.connect(self.stop_output)
        self.output_timer.timeout.connect(self.enable_output_button)
        test_button.clicked.connect(self.initialise)


    def initialise(self):

        print("drawing graph")
        # clear graph and plot data signal
        self.canvas.axes1.clear()

        df = pd.read_csv("Simulation/output00/output/200_100.current.txt",  header=None, delimiter='\t', names=['time', 'current', 'current2'])

        df.set_index('time', inplace=True)

        print(df)

        self.canvas.axes1.plot(df)

        # plot
        self.canvas.draw()

    def show_filename(self, file):
        self.data_location.setText(file)

    def clear(self):
        print("clear")

    def start_simulation(self):
        self.start_simulation_button.setEnabled(False)
        subprocess.call(["python", "Simulation/makeGrids.py"])
        self.p = subprocess.Popen(["python", "Simulation/IC.py"])

    def stop_simulation(self):
        self.p.terminate()
        self.start_simulation_button.setEnabled(True)

    def visualise(self):
        os.popen("vmd Simulation/confine.dx Simulation/output00/run.psf Simulation/output00/output/run.dcd")

    def start_output(self):
        self.output_timer.start()
        #print("timer started")
        self.start_output_button.setEnabled(False)
        os.popen("vmd -dispdev text < Simulation/output00/output/dnaloc.tcl")
        self.o = subprocess.Popen(["mpirun", "-n", "4", "python", "Simulation/output00/output/sem_analysis.py"])

    def stop_output(self):
        self.o.terminate()
        self.start_output_button.setEnabled(True)

    def enable_output_button(self):
        if self.o.poll() == 0:
            self.start_output_button.setEnabled(True)
            self.output_ready.emit()
            self.output_timer.stop()

