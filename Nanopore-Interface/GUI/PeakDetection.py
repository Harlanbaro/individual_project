from PyQt5 import QtWidgets as qtw
from PyQt5.QtWidgets import QFileDialog, QProgressBar
from click import progressbar
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import pyabf
from scipy.signal import find_peaks
import numpy as np
import numpy.polynomial.polynomial as poly
import csv

from sympy import maximum
from MplCanvas import MplCanvas


class PeakDetection(qtw.QWidget):

    def __init__(self, parent):

        # data arrays
        self.current = []
        self.time = []
        self.polyfit = []
        
        self.peak_amplitude = []
        self.peak_positions = []
        
        self.minima_amplitude = []
        self.minima_positions = []
        
        # internal variables
        self.mean = 0
        self.variance = 0
        self.sigma = 0
    
        # tab layout
        super(qtw.QWidget, self).__init__(parent)
        layout = qtw.QVBoxLayout(self)

        # TITLE
        title = qtw.QLabel("Nanopore Peak Detection, normal distribution full hight half maximum thresholds")
        title.setFixedHeight(24)

        # Peak detection
        peak_detection = qtw.QGroupBox("File Input / Output ")
        peak_detection.setFixedHeight(100)
        detection_grid = qtw.QGridLayout()
        
        # file input
        data_label = qtw.QLabel("Data Input File (.abf):")
        data_button = qtw.QPushButton("Browse")
        self.data_location = qtw.QLabel()
        self.data_location.setStyleSheet("border: 1px solid black;")

        # file input
        output_label = qtw.QLabel("Output Directory:")
        output_button = qtw.QPushButton("Browse")
        self.output_location = qtw.QLabel()
        self.output_location.setStyleSheet("border: 1px solid black;")

        # add to detection grid
        detection_grid.addWidget(data_label, 0, 0)
        detection_grid.addWidget(self.data_location, 0, 1, 1, 2)
        detection_grid.addWidget(data_button, 0, 3)

        detection_grid.addWidget(output_label, 1, 0)
        detection_grid.addWidget(self.output_location, 1, 1, 1, 2)
        detection_grid.addWidget(output_button, 1, 3)
        
        peak_detection.setLayout(detection_grid)

        # DATA BOX - includes graph parameters and results
        data = qtw.QHBoxLayout()

        # PARAMETER BOX
        parameters = qtw.QGroupBox("Parameters ")
        parameters.setFixedHeight(128)
        parameter_grid = qtw.QGridLayout()

        # peaks selection
        selection_label = qtw.QLabel("peak selection:")
        self.positive_checkbox = qtw.QCheckBox("peaks")
        self.negative_checkbox = qtw.QCheckBox("minima")
        self.positive_checkbox.setChecked(True)
        self.negative_checkbox.setChecked(True)

        # prominence
        peak_prominence_label = qtw.QLabel("peak prominence:")
        self.peak_prominence_value = qtw.QLineEdit("200")
        minima_prominence_label = qtw.QLabel("minima prominence:")
        self.minima_prominence_value = qtw.QLineEdit("200")

        # add to parameter box
        parameter_grid.addWidget(selection_label, 0, 0)
        parameter_grid.addWidget(self.positive_checkbox, 0, 1)
        parameter_grid.addWidget(self.negative_checkbox, 0, 2)
        parameter_grid.addWidget(peak_prominence_label, 1, 0)
        parameter_grid.addWidget(self.peak_prominence_value, 1, 1, 1, 2)
        parameter_grid.addWidget(minima_prominence_label, 2, 0)
        parameter_grid.addWidget(self.minima_prominence_value, 2, 1, 1, 2)
        parameters.setLayout(parameter_grid)

        # RESULTS BOX
        results = qtw.QGroupBox("Results ")
        results.setFixedHeight(128)
        results_grid = qtw.QGridLayout()

        # peaks totals and current averages
        self.total_events_label = qtw.QLabel("total events:")
        self.total_events_value = qtw.QLabel("0")
        self.total_peaks_label = qtw.QLabel("total peaks:")
        self.total_peaks_value = qtw.QLabel("0")
        self.total_minima_label = qtw.QLabel("total minima:")
        self.total_minima_value = qtw.QLabel("0")
        self.mean_current_label = qtw.QLabel("mean current:")
        self.mean_current_value = qtw.QLabel("0")
        self.sigma_label = qtw.QLabel("standard deviation:")
        self.sigma_value = qtw.QLabel("0")
        self.variance_label = qtw.QLabel("variance:")
        self.variance_value = qtw.QLabel("0")

        # add to results box
        results_grid.addWidget(self.total_events_label, 0, 0)
        results_grid.addWidget(self.total_events_value, 0, 1)
        results_grid.addWidget(self.total_peaks_label, 1, 0)
        results_grid.addWidget(self.total_peaks_value, 1, 1)
        results_grid.addWidget(self.total_minima_label, 2, 0)
        results_grid.addWidget(self.total_minima_value, 2, 1)
        results_grid.addWidget(self.mean_current_label, 0, 2)
        results_grid.addWidget(self.mean_current_value, 0, 3)
        results_grid.addWidget(self.sigma_label, 1, 2)
        results_grid.addWidget(self.sigma_value, 1, 3)
        results_grid.addWidget(self.variance_label, 2, 2)
        results_grid.addWidget(self.variance_value, 2, 3)

        results.setLayout(results_grid)

        # add results and params to box
        data.addWidget(parameters, 1)
        data.addWidget(results, 1)

        # GRAPH
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100, subplots=1)
        canvas_toolbar = NavigationToolbar(self.canvas, self)

        # export button
        export_button = qtw.QPushButton("Export Data (csv)")
        
        # toolbar layout
        toolbar = qtw.QHBoxLayout()
        toolbar.addWidget(canvas_toolbar)
        toolbar.addWidget(export_button)

        # progress bar
        self.progressbar = qtw.QProgressBar(maximum=7)
        self.progressbar.setFixedHeight(15)

        # layout
        layout.addWidget(title)
        layout.addWidget(peak_detection)
        layout.addLayout(data)
        layout.addLayout(toolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.progressbar)
        self.setLayout(layout)

        # scatters that will update later
        temp = [1,1]
        self.peaks = self.canvas.axes1.scatter(temp, temp, color='r',s=15, marker='D', alpha=1)
        self.minima = self.canvas.axes1.scatter(temp, temp, color='g', s=15, marker='D', alpha=1)
        self.peaks.set_visible(False)
        self.minima.set_visible(False)

        # connections
        self.positive_checkbox.stateChanged.connect(self.update_graph)
        self.negative_checkbox.stateChanged.connect(self.update_graph)
        self.peak_prominence_value.editingFinished.connect(self.update_graph)
        self.minima_prominence_value.editingFinished.connect(self.update_graph)
        data_button.clicked.connect(self.initialise)
        output_button.clicked.connect(self.set_output_folder)
        export_button.clicked.connect(self.export)

    def set_output_folder(self):
        folder = QFileDialog.getExistingDirectory()

        if folder:
            self.output_location.setText(folder)

    def export(self):
        
        if self.output_location.text():

            print("EXPORTING")

            # details 
            details_path = self.output_location.text() + "/details.txt"
            details = open(details_path, "w")
            details.write("some stuff")
            details.close()

            # peaks
            peaks_path = self.output_location.text() + "/peaks.csv"
            peaks = open(peaks_path, "w")
            writer = csv.writer(peaks)
            header = ["idx", "amplitude (pA)"]
            writer.writerow(header)
            
            idx = []
            for position in self.peak_positions:
                i = np.where(self.time == position)
                idx.append(i[0][0])

            t = 0
            for i in idx:
                temp = [t, self.current[i] - self.polyfit[i]]
                writer.writerow(temp)
                t += 1

            peaks.close()

            #minima
            minima_path = self.output_location.text() + "/minimas.csv"
            minima = open(minima_path, "w")
            writer = csv.writer(minima)
            header = ["idx", "amplitude (pA)"]
            writer.writerow(header)
            
            idx = []
            for position in self.minima_positions:
                i = np.where(self.time == position)
                idx.append(i[0][0])

            t = 0
            for i in idx:
                temp = [t, self.current[i] - self.polyfit[i]]
                writer.writerow(temp)
                t += 1

            peaks.close()

    def initialise(self):
        
        # initialise progress bar
        self.progressbar.setValue(0)

        response, ok = QFileDialog.getOpenFileName(self)

        if response:
            self.data_location.setText(response)

            abf = pyabf.ABF(response)
            #print(abf.headerText)
            abf.setSweep(0)            

            # send data to all other tabs
            self.time = abf.sweepX 
            self.progressbar.setValue(1)
            self.current = abf.sweepY
            self.progressbar.setValue(2)

            # clear graph and plot data signal
            self.canvas.axes1.clear()
            self.canvas.axes1.set_xlabel("Time (s)")
            self.canvas.axes1.set_ylabel("Current (pA)")
            self.canvas.axes1.plot(self.time, self.current, alpha=0.5)

            # calculate normal distribution results
            self.mean = np.mean(self.current)
            self.variance = np.var(self.current)
            self.sigma = np.sqrt(self.variance)

            # plot poly line of best fit
            coefficients = poly.polyfit(self.time, self.current, 8)
            self.polyfit = poly.polyval(self.time, coefficients)
            self.canvas.axes1.plot(self.time, self.polyfit, color='b', alpha=0.5)

            self.update_graph()
        
    def update_graph(self):
        
        # initialise progress bar
        self.progressbar.setValue(3)

        # update values on screen
        self.mean_current_value.setNum(self.mean)
        self.sigma_value.setNum(self.sigma)
        self.variance_value.setNum(self.variance)

        # remove old lines
        self.peaks.set_visible(False)
        self.minima.set_visible(False)
        
        self.progressbar.setValue(4)

        # peak detection
        if self.positive_checkbox.isChecked():
            peaks = find_peaks(self.current, prominence=float(self.peak_prominence_value.text()))
            
            # check there are peaks before trying to plot to prevent crashing
            if(len(peaks[0]) > 1): 
                self.peak_amplitude = self.current[peaks[0]]
                self.peak_positions = self.time[peaks[0]]
                self.peaks = self.canvas.axes1.scatter(self.peak_positions, self.peak_amplitude, color='r', s=15, marker='D')
                
                # update total
                total = len(peaks[0])
                self.total_peaks_value.setText(str(total))
            else:
                self.total_peaks_value.setText("0")

        else:
            self.total_peaks_value.setText("0")

        self.progressbar.setValue(5)

        if self.negative_checkbox.isChecked():
            minima = find_peaks(self.current * -1, prominence=float(self.minima_prominence_value.text()))
            
            # check there are peaks before trying to plot to prevent crashing
            if(len(minima[0]) > 1): 
                self.minima_amplitude = self.current[minima[0]]
                self.minima_positions = self.time[minima[0]]
                self.minima = self.canvas.axes1.scatter(self.minima_positions, self.minima_amplitude, color='g', s=15, marker='D')
                
                # update total
                total = len(minima[0])
                self.total_minima_value.setText(str(total))
            else:
                self.total_minima_value.setText("0")
        else:
            self.minima = self.canvas.axes1.scatter(0, 0, color='g', s=15, marker='D', alpha=1)
            self.total_minima_value.setText("0")

        self.progressbar.setValue(6)

        # update grand total
        total_events = int(self.total_peaks_value.text()) + int(self.total_minima_value.text())
        self.total_events_value.setText(str(total_events))

        # plot
        self.canvas.draw()
        self.canvas.flush_events()

        self.progressbar.setValue(7)
